<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use App\Match;
use App\Game;
use App\Swipe;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $swipedUsers = Swipe::where('user_id', '=', Auth::id())
            ->pluck('partner_id')
            ->merge([6])
            ->toArray();

        $user = User::whereNotIn('id', $swipedUsers)->take(1)->get();
    	
    	return view('home', compact('user'));
    }

    public function profile()
    {
    	return view('profile');
    }

    protected function getGameList($steamID)
    {
        $client = new Client();
        $res = $client -> request('GET', 'http://api.steampowered.com/IPlayerService/GetOwnedGames/v0001/?key='.env('STEAM_API_KEY', '').'&steamid=' . $steamID . '&include_appinfo=1&format=json');

        return json_decode($res->getBody()->getContents());
    }
}
