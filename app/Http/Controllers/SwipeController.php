<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use Illuminate\Http\Request;

class SwipeController extends Controller
{
    public function store(Request $request, User $user)
    {
		$request->validate([
			'action' => 'required',
		]);

		Auth::user()->swipes()->create([
			'partner_id' => $user->id,
			'is_positive' => $request->action
		]);

		if ($request->action == 1 && $user->swipes()->where('partner_id', '=', Auth::id())->count()) {
			Auth::user()->matches()->create([
				'user_id' => Auth::id(),
				'partner_id' => $user->id
			]);

			$user->matches()->create([
				'user_id' => $user->id,
				'partner_id' => Auth::id()
			]);

			return redirect()->back()->with('match', 'true');
		} else {
			return redirect()->back();
		}
    }
}
