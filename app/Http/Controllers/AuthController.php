<?php
namespace App\Http\Controllers;

use Auth;
use App\User;
use App\Game;
use GuzzleHttp\Client;
use Invisnik\LaravelSteamAuth\SteamAuth;

class AuthController extends Controller
{
    /**
     * The SteamAuth instance.
     *
     * @var SteamAuth
     */
    protected $steam;

    /**
     * The redirect URL.
     *
     * @var string
     */
    protected $redirectURL = '/';

    /**
     * AuthController constructor.
     * 
     * @param SteamAuth $steam
     */
    public function __construct(SteamAuth $steam)
    {
        $this->steam = $steam;
    }

    /**
     * Redirect the user to the authentication page
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function redirectToSteam()
    {
        return $this->steam->redirect();
    }

    /**
     * Get user info and log in
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function handle()
    {
        if ($this->steam->validate()) {
            $info = $this->steam->getUserInfo();

            if (!is_null($info)) {
                $user = $this->findOrNewUser($info);

                Auth::login($user, true);

                return redirect($this->redirectURL); // redirect to site
            }
        }
        return $this->redirectToSteam();
    }

    /**
     * Getting user by info or created if not exists
     *
     * @param $info
     * @return User
     */
    protected function findOrNewUser($info)
    {
        $user = User::where('steamid', $info->steamID64)->first();

        if (!is_null($user)) {
            return $user;
        }

        $user = User::create([
            'username' => $info->personaname,
            'avatar' => $info->avatarfull,
            'steamid' => $info->steamID64
        ]);

        $this->saveGames($user);
        $this->saveFriends($user);

        return $user;
    }

    protected function saveGames($user) {
        $gameList = $this->getGameList($user->steamid)->response->games;

        foreach ($gameList as $game) {
            $gameModel = Game::firstOrCreate([
                'appid' => $game->appid
            ],
            [
                'img_icon_url' => $game->img_icon_url,
                'img_logo_url' => $game->img_logo_url,
                'name' => $game->name
            ]);

            $user->games()->syncWithoutDetaching([
                $gameModel->id => [
                    'playtime_2weeks' => 0,
                    'playtime_forever' => $game->playtime_forever,
                    'is_favorite' => false
                ]
            ]);
        }
    }

    protected function saveFriends($user)
    {
        $friendList = collect($this->getFriendList($user->steamid)->friendslist->friends);

        $friendListFormatted = $friendList->map(function ($friend) {
            return [ 'steamid' => $friend->steamid ];
        });

        $user->friends()->createMany($friendListFormatted->toArray());
    }

    protected function getFriendList($steamID)
    {
        $client = new Client();
        $res = $client -> request('GET', 'http://api.steampowered.com/ISteamUser/GetFriendList/v0001/?key='.env('STEAM_API_KEY', '').'&steamid='. $steamID .'&relationship=friend');

        return json_decode($res->getBody()->getContents());
    }

    protected function getGameList($steamID)
    {
        $client = new Client();
        $res = $client -> request('GET', 'http://api.steampowered.com/IPlayerService/GetOwnedGames/v0001/?key='.env('STEAM_API_KEY', '').'&steamid=' . $steamID . '&include_appinfo=1&format=json');

        return json_decode($res->getBody()->getContents());
    }
}
