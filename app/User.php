<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    // use Notifiable;

    protected $guarded = [
        'id', 'remember_token'
    ];

    protected $hidden = [
        'remember_token'
    ];

    public function games()
    {
        return $this->belongsToMany('App\Game');
    }

    public function friends()
    {
        return $this->hasMany('App\Friend');
    }

    public function matches()
    {
        return $this->hasMany('App\Match');
    }

    public function swipes()
    {
        return $this->hasMany('App\Swipe');
    }
}
