<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Match extends Model
{
	protected $guarded = ['id'];

    /*public function user()
    {
    	return $this->belongsTo('App\User', 'user_id');
    }*/

    public function partner()
    {
    	return $this->belongsTo('App\User', 'partner_id');
    }
}
