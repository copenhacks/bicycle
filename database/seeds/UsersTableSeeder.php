<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
	        [
	        	'id' => 1, 
		        'steamid' => '76561198051235002',
		        'username' => 'PartyMaker*',
		        'avatar' => 'https://steamcdn-a.akamaihd.net/steamcommunity/public/images/avatars/a3/a367182e7a239f10662dd2649e9adc6683ec43ab_full.jpg',
		        'country' => 'LT',
		        'region' => 'EU',
		        'description' => 'Another good quote.',
		        'created_at' => \Carbon\Carbon::now(),
		        'updated_at' => \Carbon\Carbon::now()
			],
	        [
	        	'id' => 2, 
		        'steamid' => '76561198090744460',
		        'username' => 'Destrian',
		        'avatar' => 'https://steamcdn-a.akamaihd.net/steamcommunity/public/images/avatars/11/1193d9cfacc1244e66c0521df350b4414153596f_full.jpg',
		        'country' => 'CA',
		        'region' => 'EU',
		        'description' => 'I am just a nice person.',
		        'created_at' => \Carbon\Carbon::now(),
		        'updated_at' => \Carbon\Carbon::now()
			],
	        [
	        	'id' => 3, 
		        'steamid' => '76561197988271944',
		        'username' => 'twitch.tv/vulcanraven89',
		        'avatar' => 'https://steamcdn-a.akamaihd.net/steamcommunity/public/images/avatars/19/1995deba7f613f0d7c6249d7b2a59dd8747e44e6_full.jpg',
		        'country' => 'IT',
		        'region' => 'EU',
		        'description' => 'No description.',
		        'created_at' => \Carbon\Carbon::now(),
		        'updated_at' => \Carbon\Carbon::now()
			],
	        [
	        	'id' => 4, 
		        'steamid' => '76561198128430714',
		        'username' => 'Dreamas',
		        'avatar' => 'https://steamcdn-a.akamaihd.net/steamcommunity/public/images/avatars/70/70f781fe21f33cdbe62d2329d655d43c3f2ec5ee_full.jpg',
		        'country' => 'LT',
		        'region' => 'EU',
		        'description' => 'No idea.',
		        'created_at' => \Carbon\Carbon::now(),
		        'updated_at' => \Carbon\Carbon::now()
			],
	        [
	        	'id' => 5, 
		        'steamid' => '76561198013229072',
		        'username' => 'breAker-',
		        'avatar' => 'https://steamcdn-a.akamaihd.net/steamcommunity/public/images/avatars/c0/c050f85848a282e17a97e2e1edd070aff3b2b035_full.jpg',
		        'country' => 'AE',
		        'region' => 'EU',
		        'description' => 'No idea.',
		        'created_at' => \Carbon\Carbon::now(),
		        'updated_at' => \Carbon\Carbon::now()
			],
        ];

        User::insert($users);
    }
}
