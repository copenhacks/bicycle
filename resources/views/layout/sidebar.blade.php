    <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <ul class="sidebar-menu sm-hidden">
            <li class="header">YOUR GAMER MATCHES:</li>
            @forelse (Auth::user()->matches as $user)
                <li>
                    <a href="{{ route('profile.show', $user->partner->id) }}">
                        <div class="row">
                            <div class="col-md-4"><img src="{{$user->partner->avatar}}" class="img-circle img-responsive" alt="User Image"></div>
                            <div class="col-md-8">
                                <div class="pull-left info sm-hidden">
                                    <p><b>{{ $user->partner->username }}</b></p>
                                    @if (session('match'))
                                        <span class="text-danger">New match</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </a>
                </li>
            @empty
                <li class="nav-item ml-3"><a href="">You are forever alone.</a></li>
            @endforelse
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
