@extends('layout.default')

@section('content')
    <div class="container-fluid">
        <div class="row">
            @include('layout.sidebar')

    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-md-offset-3 col-md-6">
                    @if (session('match'))
                    <div class="alert alert-success">
                        <center>There is a match!</center>
                    </div>
                    @endif
                    <div class="wrap">
                        <!-- start jtinder container -->
                        <div id="tinderslide">
                            <ul>
                                <!-- panel start -->
                                <li class="pane1">
                                    <form id="dislikeForm" action="{{ route('swipe', $user[0]->id) }}" method="post" class="hidden">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="action" value="0">
                                    </form>
                                    <form id="likeForm" action="{{ route('swipe', $user[0]->id) }}" method="post" class="hidden">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="action" value="1">
                                    </form>
                                    <div class="box box-widget widget-user" style="height: 100%;">
                                        <!-- Add the bg color to the header using any of the bg-* classes -->
                                        <div class="widget-user-header bg-purple">
                                            <h3 class="widget-user-username">{{ $user[0]->username }}<br>
                                                <p style="font-size: 12px;color:white"><i class="fa fa-circle text-success"></i> Online</p>
                                            </h3>
                                        </div>

                                        <div class="widget-user-image">
                                            <img class="img-circle" src="{{ $user[0]->avatar }}" alt="User Avatar">
                                        </div>
                                        <div class="box-footer">
                                            <div class="row">
                                                <div class="col-sm-4 border-right">
                                                    <div class="description-block">
                                                        <h5 class="description-header">Favorite Games</h5>
                                                        <p style="font-size: 12px;">
                                                            <span class="description-text">CS GO</span></br>
                                                            <span class="description-text">League of Legend</span></br>
                                                            <span class="description-text">Rocket League</span></br>
                                                            <span class="description-text">GTA</span></br>
                                                        </p>
                                                    </div>
                                                    <!-- /.description-block -->
                                                </div>
                                                <!-- /.col -->
                                                <div class="col-sm-4 border-right">
                                                    <div class="description-block">
                                                        <h5 class="description-header">Languages</h5>
                                                        <p style="font-size: 12px;">
                                                            <span class="description-text">ru, lv, en</span></br>
                                                        </p>
                                                    </div>
                                                    <!-- /.description-block -->
                                                </div>
                                                <!-- /.col -->
                                                <div class="col-sm-4">
                                                    <div class="description-block">
                                                        <h5 class="description-header">Country</h5>
                                                        <p style="font-size: 12px;">
                                                            <span class="description-text">{{$user[0]->country}}</span></br>
                                                        </p>
                                                        <span class="description-text"></span>
                                                    </div>
                                                    <!-- /.description-block -->
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="description-block">
                                                        <h5 class="description-header">Description</h5>
                                                        <p style="font-size: 12px;">
                                          <span class="description-text">{{ $user[0]->description }}
                                          </span></br>
                                                        </p>
                                                        <span class="description-text"></span>
                                                    </div>
                                                    <!-- /.description-block -->
                                                </div>
                                                <!-- /.col -->
                                            </div>
                                            <!-- /.row -->
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <!-- end jtinder container -->
                    </div>
                    <!-- end padding container -->
                    <!-- jTinder trigger by buttons  -->
                    <div class="actions">
                        <a href="#" class="dislike"><i></i></a>
                        <a href="#" class="like"><i></i></a>
                    </div>
                </div>
            </div>
            <!-- /.row (main row) -->

        </section>

    </div>
@endsection
