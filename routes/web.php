<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['web', 'auth']], function () {
	Route::get('/', 'HomeController@index')->name('home');
	
	Route::get('profile', 'HomeController@profile')->name('profile');
	Route::get('profile/{user}', 'ProfileController@show')->name('profile.show');

	Route::post('swipe/{user}', 'SwipeController@store')->name('swipe');

	Route::get('matches', 'SwipeController@new')->name('matches');

	Route::post('logout', 'Auth\LoginController@logout')->name('logout');
});

Route::get('login', 'GuestController@index')->name('login');

Route::get('auth/steam', 'AuthController@redirectToSteam')->name('auth.steam');
Route::get('auth/steam/handle', 'AuthController@handle')->name('auth.steam.handle');
